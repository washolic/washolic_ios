//
//  ServiceApiEndPoints.swift
//  EduLights
//
//  Created by Abdul Muqeem on 22/03/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation

class ServiceApiEndPoints: NSObject  {

    static let baseURL = "http://edulights.com/edulights/api/"
    
    // User Module
    static let login = baseURL + "login"
    static let forgotPassword = baseURL + "forgotpassword"
    static let register = baseURL + "register"
    static let educationList = baseURL + "geteducation"

    static let notificationList = baseURL +  "getnotifications"
    static let updateDeviceToken = baseURL +  "update/deviceToken"
    static let bannerImage = baseURL +  "advertise/image"
    static let courseList = baseURL +  "course/list"
    static let instituteList = baseURL +  "search"
    static let instituteDetail = baseURL +  "institute/details"
    static let courseDetail = baseURL +  "course/details"
    static let batchDetail = baseURL +  "course/batch"
    static let sendRequest = baseURL +  "create/request"
    
    static let updateProfile = baseURL + "profile/update"
    static let notificationStatus = baseURL + "changepushnotificationstatus"
    static let changePassword = baseURL + "changepassword"
    static let contacttUs = baseURL + ""
    static let about = baseURL +  "aboutus"
    static let help = baseURL +  "help"
    static let terms = baseURL +  "termsConditions"
    static let privacy = baseURL +  "privacyPolicy"
    
    static let institute = baseURL + "institutes"
    static let location = baseURL + "locations"
    static let filter = baseURL + "filter"
    static let payment = baseURL + "create/payment"
    static let emailToken = baseURL + "send/token"
    static let verifyEmailToken = baseURL + "verify/token"
    
}
