//
//  BaseUIView.swift
//  Washolic
//
//  Created by Abdul Muqeem on 10/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class BaseUIView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cornerRadius = 12.0
        shadow = true
        BorderColor = UIColor.red
        BorderWidth = 2.0

    }
    
}
