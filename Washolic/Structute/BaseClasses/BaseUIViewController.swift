//
//  BaseUIViewController.swift
//  Washolic
//
//  Created by Abdul Muqeem on 10/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class BaseUIViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // Navigation Bar Methods
    
    func HideNavigationBar() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func ShowNavigationBar() {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func SideMenuButtonNavigationBar(title:String) {
        
        self.title = title
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    func BackButtonNavigationBar(title:String) {
        
        self.title = title
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
