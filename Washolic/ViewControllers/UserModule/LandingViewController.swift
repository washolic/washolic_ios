//
//  LandingViewController.swift
//  Washolic
//
//  Created by Abdul Muqeem on 10/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class LandingViewController: BaseUIViewController {

    class func instantiateFromStoryboard() -> LandingViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LandingViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.HideNavigationBar()
        
    }

}
