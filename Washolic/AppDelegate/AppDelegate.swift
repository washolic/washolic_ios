//
//  AppDelegate.swift
//  Washolic
//
//  Created by Abdul Muqeem on 10/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        IQKeyboardManager.shared.enable = true
        self.NavigateTOInitialViewController()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }

    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func NavigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserManager.isUserLogin() {
            
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            self.window?.rootViewController = controller
            
        } else {
            
            let nav = RootViewController.instantiateFromStoryboard()
            self.window?.rootViewController = nav
            let vc = LandingViewController.instantiateFromStoryboard()
            nav.pushViewController(vc, animated: true)
            
        }
    }

}

